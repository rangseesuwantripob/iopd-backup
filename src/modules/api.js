const axios = require("axios");
var qs = require('qs');
export default {
  BASE_URL: "https://iopdapi.ml/?function=",
  // BASE_URL: "http://iopdapi.ci/?function=",
  post: function(apiname, params) {
    var self = this;
    const option = {
      method: "POST",
      url: self.BASE_URL + apiname,
      data: qs.stringify(params)
    };
    return axios(option).then(function(response) {
      return response.data;
    });
  },
  postMultipart: function(apiname, formData){
    var self = this;
    return axios.post(self.BASE_URL + apiname, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(function(response) {
      return response.data;
    });
  }
};
