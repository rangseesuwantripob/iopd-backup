export default {
  toggleSideBar: function() {
    $("#layout").toggleClass("custom-sidebar-close");
  },
  isFunction: function(functionToCheck) {
    return (
      functionToCheck &&
      {}.toString.call(functionToCheck) === "[object Function]"
    );
  },
  loaderOn: function(){
    $("#loader").addClass('active');
  },
  loaderOff: function(){
    $("#loader").removeClass('active');
  },
  isEmail: function(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email == '' || !re.test(email))
    {
        return false;
    }
    return true;
  }
};
