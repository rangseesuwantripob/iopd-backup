import api from "./api";

const getPlaceList = async () => {
  const res = await api.post("getPlaceList");
  return res.results;
};
const getAppointmentList = async (patient_id,employee_id) => {
  const params = {
    patient_id: patient_id,
    employee_id: employee_id
  };
  const res = await api.post("getAppointmentList", params);
  return res.results;
};
const getAppointmentListByDate = async (date) => {
  const params = {
    date: date
  };
  const res = await api.post("getAppointmentListByDate", params);
  return res.results;
};
const getDepartmentList = async () => {
  const res = await api.post("getDepartmentList");
  return res.results;
};
const getRoomList = async () => {
  const res = await api.post("getRoomList");
  return res.results;
};
const getActivityList = async () => {
  const res = await api.post("getActivityList");
  return res.results;
};
const loginPatient = async (username, password) => {
  const params = {
    username: username,
    password: password
  };
  const res = await api.post("loginPatient", params);
  return res.results;
};
const getQueueStatusList = async () => {
  const res = await api.post("getQueueStatusList");
  return res.results;
};
const getQueueTypeList = async () => {
  const res = await api.post("getQueueTypeList");
  return res.results;
};
const getQueueList = async (queue_type_id,queue_status_id) => {
  const params = {
    queue_type_id: queue_type_id,
    queue_status_id: queue_status_id
  };
  const res = await api.post("getQueueList", params);
  return res.results;
};
const changeQueueStatus = async (queue_id,queue_status_id) => {
  const params = {
    queue_id: queue_id,
    new_queue_status_id: queue_status_id
  };
  const res = await api.post("changeQueueStatus", params);
  return res.results;
};
const changeQueueType = async (queue_id,new_queue_type_id,workflowId) => {
  const params = {
    queue_id: queue_id,
    new_queue_type_id: new_queue_type_id,
    workflowId: workflowId
  };
  const res = await api.post("changeQueueType", params);
  return res.results;
};
const toNextQueueType = async (queue_id) => {
  const params = {
    queue_id: queue_id
  };
  const res = await api.post("toNextQueueType", params);
  return res.results;
};
const getTimeSlotList = async () => {
  const res = await api.post("getTimeSlotList");
  return res.results;
}
const getPatientHint = async (str) => {
  const params = {
    str: str
  };
  const res = await api.post("getPatientHint", params);
  return res.results;
}
const getPatientList = async (str) => {
  const params = {
    str: str
  };
  const res = await api.post("getPatientList", params);
  return res.results;
}
const getDoctorHint = async (str) => {
  const params = {
    str: str
  };
  const res = await api.post("getDoctorHint", params);
  return res.results;
}
const getWorkflowList = async () => {
  const res = await api.post("getWorkflowList");
  return res.results;
}
const getNumberEachSubProcess = async (workflowId) => {
  const params = {
    workflowId: workflowId
  };
  const res = await api.post("getNumberEachSubProcess", params);
  return res.results;
}
const addAppointment = async (patient_id,employee_id,date,timeslot_id,workflowId,room_id) => {
  const params = {
    patient_id: patient_id,
    employee_id: employee_id,
    date: date,
    timeslot_id: timeslot_id,
    workflowId: workflowId,
    room_id: room_id
  };
  const res = await api.post("addAppointment", params);
  return res.results;
}
const addQueue = async (patient_id,room_id,appointment_id,queue_type_id,workflow_id) => {
  const params = {
    patient_id: patient_id,
    room_id: room_id,
    appointment_id: appointment_id,
    queueTypeId: queue_type_id,
    queue_status_id: 1,
    workflowId: workflow_id

  };
  const res = await api.post("addQueue", params);
  return res.results;
}
const addDepartment = async (name,description) => {
  const params = {
    name: name,
    description: description
  };
  const res = await api.post("addDepartment", params);
  return res.results;
}
const addRoom = async (name,description, department_id,isroom) => {
  const params = {
    name: name,
    description: description,
    departmentId: department_id,
    isRoom:isroom
  };
  const res = await api.post("addRoom", params);
  return res.results;
}
const addTimeSlot = async (startTime,endTime,maxCapacity,roomListId) => {
  const params = {
    startTime: startTime,
    endTime: endTime,
    maxCapacity: maxCapacity,
    roomListId: roomListId
  };
  const res = await api.post("addTimeSlot", params);
  return res.results;
}

const setHospitalInfo = async (hospitalname, latitude, longitude, distance) => {
  const params = {
    hospitalName: hospitalname,
    latitude: latitude,
    longitude: longitude,
    distance: distance
  }
  console.log("service: "+params.Latitude+"   "+params.Longitude);
  const res = await api.post("setHospitalInfo", params);
  return res.results;
}
const registerEmployee = async (firstname,surname,username,password,email,is_doctor,department_id) => {
  const params = {
    firstname: firstname,
    surname: surname,
    username: username,
    password: password,
    email: email,
    is_doctor: is_doctor,
    department_id: department_id
  };
  const res = await api.post("registerEmployee", params);
  return res.results;
}
const registerPatient = async (firstname,surname,username,password,email) => {
  const params = {
    firstname: firstname,
    surname: surname,
    username: username,
    password: password,
    email: email
  };
  const res = await api.post("registerPatient", params);
  return res.results;
}
const sendnotidata = async(token, title, message) => {
  const params = {
    token: token,
    title: title,
    message: message
  };
  const res = await api.post("SendNotificationData", params);
  return res.results;
}
const getPatientToken = async(queue_id) => {
  const params = {
    queue_id: queue_id
  };
  const res = await api.post("getPatientToken", params);
  return res.results;
}
const IOread = async(importfile) => {
  var formData = new FormData();
  formData.append("importfile", importfile);
  const res = await api.postMultipart("IOread", formData);
  return res.results;
}
const getDoctorListFromDepartmentId = async(department_id) => {
  const params = {
    department_id: department_id
  };
  const res = await api.post("getDoctorListFromDepartmentId", params);
  return res.results;
}
const getRoomListFromDepartmentId = async(department_id) => {
  const params = {
    department_id: department_id
  };
  const res = await api.post("getRoomListFromDepartmentId", params);
  return res.results;
}
const getTimeslotListFromRoomId = async(department_id, roomId) => {
  const params = {
    departmentId: department_id,
    roomListId: roomId
  };
  const res = await api.post("getTimeslotListFromRoomId", params);
  return res.results;
}
const addRoomSchedule = async( roomId,employee_id,date,timeslot_id,workflowId) => {
  const params = {
    room_id: roomId,
    employee_id: employee_id,
    date: date,
    timeslot_id: timeslot_id,
    workflow_id: workflowId
  };
  const res = await api.post("addRoomSchedule", params);
  return res.results;
}

export default {

  getPlaceList,
  getAppointmentList,
  getAppointmentListByDate,
  getDepartmentList,
  getRoomList,
  getActivityList,
  loginPatient,
  getQueueStatusList,
  getQueueTypeList,
  getQueueList,
  getNumberEachSubProcess,
  changeQueueStatus,
  changeQueueType,
  toNextQueueType,
  getTimeSlotList,
  getPatientHint,
  getDoctorHint,
  getWorkflowList,
  addAppointment,
  addQueue,
  addDepartment,
  addRoom,
  addTimeSlot,
  setHospitalInfo,
  getPatientList,
  sendnotidata,
  getPatientToken,
  IOread,
  registerEmployee,
  registerPatient,
  getRoomListFromDepartmentId,
  getDoctorListFromDepartmentId,
  getTimeslotListFromRoomId,
  addRoomSchedule
};
