// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueDraggable from 'vue-draggable'
import { VueContext } from 'vue-context';
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './App'
import router from './router'
import Modules from '@/modules'
import './assets/plugins/bootstrap-notify.js'
import './assets/plugins/perfect-scrollbar.jquery.min.js'
import './assets/Template/assets/js/now-ui-dashboard.js'

Vue.use(Modules)
Vue.use(BootstrapVue)
Vue.use(VueDraggable)
Vue.use(VueContext)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAgX3c1-CxjKQOjL9gxEgdMvLOTEriWvUc',
    libraries: 'places'
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
